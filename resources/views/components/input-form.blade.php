<div {{ $attributes }}>
    <!-- I begin to speak only when I am certain what I will say is not better left unsaid. - Cato the Younger -->
    <div class="form-floating mb-3">
        <input type="{{$tipo}}" class="form-control" id="{{$idInput}}" placeholder="name@example.com">
        <label for="{{$idInput}}">{{$texto}}</label>
    </div>
</div>