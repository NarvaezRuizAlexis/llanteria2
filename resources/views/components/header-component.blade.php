<div {{$attributes}}>
    <head class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container-fluid">
                <h2 class="navbar-brand"> 
                    <i class="bi bi-tropical-storm"></i> 
                    {{$tituloSeccion}}
                </h2>
                <a href="#" class="btn btn-primary">salir</a>
            </div>
        </nav>
    </head>
</div>