<div class="{{$nombresClases}}">
    <!--Contenedor del logo-->
    <div class="content-log p-3 d-flex justify-content-center text-light">
        LOGO
    </div>
    <!--Contenedor de las opcioens del menu-->
    <div class="content-nav">
        <nav class="nav">
            <ul class="nav-meun-items list-unstyled">
                <li class="item text-light"><a href="../" class="text-decoration-none p-3 ps-1 d-flex justify-content-start align-items-center text-light"><i class="bi bi-house"></i>Inicio</a></li>
                <li class="item text-light"><a href="/clientes" class="text-decoration-none p-3 ps-1 d-flex justify-content-start align-items-center text-light"><i class="bi bi-suit-spade"></i>Clientes</a></li>
                <li class="item text-light"><a href="#" class="text-decoration-none p-3 ps-1 d-flex justify-content-start align-items-center text-light"><i class="bi bi-arrow-repeat"></i>Cambio de neumaticos</a></li>
                <li class="item text-light"><a href="#" class="text-decoration-none p-3 ps-1 d-flex justify-content-start align-items-center text-light"><i class="bi bi-journal-check"></i>Inventario</a></li>
                <li class="item text-light"><a href="#" class="text-decoration-none p-3 ps-1 d-flex justify-content-start align-items-center text-light"><i class="bi bi-person-rolodex"></i>Roles</a></li>
                <li class="item text-light"><a href="#" class="text-decoration-none p-3 ps-1 d-flex justify-content-start align-items-center text-light"><i class="bi bi-people"></i>Usuarios</a></li>
                <li class="item text-light"><a href="#" class="text-decoration-none p-3 ps-1 d-flex justify-content-start align-items-center text-light"><i class="bi bi-clipboard-check"></i>Pedidos</a></li>
                <li class="item text-light"><a href="#" class="text-decoration-none p-3 ps-1 d-flex justify-content-start align-items-center text-light" id="button-show"><i class="bi bi-chevron-right"></i>mostrar</a></li>
            </ul>
        </nav>
    </div>
</div>