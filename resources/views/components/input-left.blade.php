<div {{$attributes}}>
    <div class="row">
        @if($texto != null)
            <label for="{{$idInput}}" class="{{$colLabel}} d-flex justify-content-center align-items-center py-1">{{$texto}}</label>
        @endif
        <input type="{{$tipo}}" placeholder="{{$placeholder}}" id="{{$idInput}}" class="{{$colInput}} py-1">
    </div>
</div>