@extends('viewTemplate.viewTemplate')

@section('header-seccion')
<!--Seccion del header de la pagina-->
<x-header-component tituloSeccion="Clientes" class="container-fluid align-self-start" />
@endsection

@section('body-seccion')
<h2 class="my-3">Listado de clientes.</h2>
<p class="mb-5">Consulte los datos de los clientes.</p>

<!--Contenedor del formulario de busqueda-->
<div class="">
    <h3 class="h5 bg-dark text-white p-2 mb-3 rounded">Buscar un cliente.</h3>
    <form class="row g-4 justify-content-end">
        <div class="col-auto">
            <select class="form-select form-select-md">
                <option selected>Selecciona el campo</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>
        <x-input-left class="col-5" idInput="inputNombre" tipo="text" colInput="col-12" placeholder="buscar" />
        <div class="col-auto">
            <button type="button" class="btn btn-dark">Buscar</button>
        </div>

    </form>
</div>

<!--Contenedor de la tablas-->
<div class="container-table mt-5">
    <table class="table">
        <thead class="bg-dark rounded">
            <tr>
                <th scope="col" class="text-white text-center">ID</th>
                <th scope="col" class="text-white text-center">Campo 1</th>
                <th scope="col" class="text-white text-center">Campo 2</th>
                <th scope="col" class="text-white text-center">Campo 3</th>
                <th scope="col" class="text-white text-center">Campo 4</th>
                <th scope="col" class="text-white text-center">Campo 5</th>
                <th scope="col" class="text-white text-center">Acciones</th>
            </tr>

        </thead>
        <tbody>
            <!--Zona de las filas.-->
            <tr>
                <td>Item $</td>
                <td>Item $</td>
                <td>Item $</td>
                <td>Item $</td>
                <td>Item $</td>
                <td>Item $</td>
                <td class="row justify-content-around">
                    <button class="col-5 btn btn-outline-primary" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Borrar">
                        <i class="bi bi-pencil"></i>
                    </button>
                    <button class="col-5 btn btn-outline-danger" data-bs-toggle="tooltip" data-bs-placement="bottom" title="Borrar">
                        <i class="bi bi-trash"></i>
                    </button>
                </td>
            </tr>
        </tbody>
    </table>
</div>
@endsection