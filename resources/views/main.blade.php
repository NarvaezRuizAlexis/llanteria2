@extends('viewTemplate.rootView')


@section('header-seccion')
<!--Seccion del header de la pagina-->
<x-header-component tituloSeccion="Secccio 1" class="container-fluid align-self-start" />
@endsection

<!--Seccion del cuepo de lapagina-->
@section('body-seccion')
<div class="container-fluid">
    <h4 class="mt-3 mb-3">
        <i class="bi bi-hand-thumbs-up"></i>
        Hola. XXXX - XXXXX
    </h4>
    <div class="alert alert-success">
        <p>¡Bienvenido al sistema 00000, esperamos que tengas un buen día :)¡</p>
    </div>
</div>
@endsection

@section('footer-seccion')
<!--Seccion del footer de la pagina-->
@component('components.footerComponent')
<x-footer-component class="container mt-auto"/>
@endsection

@section('archivos-scripts')
<script src="./js/sidebar.js"></script>
@endsection