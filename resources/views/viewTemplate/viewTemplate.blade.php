@extends('viewTemplate.rootView')

@section('footer-seccion')
    <!--Seccion del footer de la pagina-->
    <x-footer-component class="container mt-auto"/>
@endsection

@section('archivos-scripts')
    <script src="./js/sidebar.js"></script>
@endsection