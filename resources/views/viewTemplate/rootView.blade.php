<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="./lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="./css/app.css">
    <title>LLanteria Nueva.</title>
</head>

<body>
    <!--El contenedor padre es el que contiene a todos -->
    <div class="container-pather-page vw-100 bg-primary vh-100 position-relative overflow-hidden">
        <!--Inicio de la barra lateral -->
        <x-side-bar nombresClases="side-bar-personalisada bg-dark vh-100 position-absolute p-3 z-index-sidebar"/>
        <!--Resto de la pagina -->
        <div class="full-body-page new-width position-absolute vh-100 bg-light rounded p-3 overflow-auto" id="full-body-page">
            <div class="container-fluid min-vh-100 d-flex flex-column ">
                <!--A qui va el resto de la pagina segun sea la vista que se elija-->
                @yield('header-seccion')
                <main class="container-fluid">
                    @yield('body-seccion')
                </main>
                @yield('footer-seccion')
            </div>
        </div>
    </div>
    <script src="./lib/bootstrap/js/bootstrap.js"></script>
    @yield('archivos-scripts')
</body>

</html>