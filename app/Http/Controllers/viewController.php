<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
/** 
 * Controlador para manejar las vistas.
 * 
 * Es provisional
 * 
 * 
*/
class viewController extends Controller
{
    public function showView($viewName){
        return view($viewName);
    }
}
