<?php

namespace App\View\Components;

use Illuminate\View\Component;

class inputLeft extends Component
{
    public $idInput;
    public $texto;
    public $tipo;
    public $placeholder;
    public $colLabel;
    public $colInput;


    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($idInput, $texto = null, $tipo, $placeholder = null,$colLabel = 'col-auto',$colInput = 'col-auto')
    {
        $this->idInput = $idInput;
        $this->texto = $texto;
        $this->tipo = $tipo;
        $this->placeholder = $placeholder;
        $this->colInput=$colInput;
        $this->colLabel=$colLabel;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input-left');
    }
}
