<?php

namespace App\View\Components;

use Illuminate\View\Component;

class inputForm extends Component
{
    public $tipo;
    public $idInput;
    public $texto;

    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct($tipo, $idInput, $texto)
    {
        //
        $this->tipo = $tipo;
        $this->idInput = $idInput;
        $this->texto = $texto;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        return view('components.input-form');
    }
}
