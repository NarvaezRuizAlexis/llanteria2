# Documentación.
**Participantes**
* Alexis Narvaez

* Omar ibañes

* Adan Sanchez

* Danni

**Título del documento:** Sistema de llantas 


# Marco contextual.

## 1.1 Nombre del proyecto.
Desarrollo de un sistema web para el control de ventas para el negocio de llantas nuevas y baterías de El llantero de Oaxaca.
## 1.2 Giro del negocio.
Comercial-micronegocio, empresa dedicada a vender llantas y baterías.
## 1.3 Antecedentes del negocio.
El llantero es un micronegocio de ventas de llantas y baterías, además ofrecen servicios para cambiar las llantas o las baterías de los automóviles.

Este micronegocio está establecido en su localidad y se encuentra ubicado en Valerio Trujano 112, Agencia de Policía San Martin Mexicapan, 68140 Oaxaca de Juárez, Oax.

Este negocio ha tenido una buena aceptación por parte de los clientes de la zona teniendo ventas aceptables en sus servicios los cuales son la venta de llantas y venta de baterías.

## 1.4 Misión.
Estamos comprometidos a proveer productos, servicios y atención de la más alta calidad, respetando y preservando nuestro entorno, brindando seguridad y confianza a nuestros clientes y proveedores.

## 1.5 Visión.
Cumplir las necesidades de nuestros clientes, permitiéndonos ser indudablemente la mejor opción del mercado de llantas y baterías para brindar una excelente atención a los automóviles.

## 1.6 Croquis de Ubicación
Valerio Trujano 112, Agencia de Policía San Martin Mexicapan, 68140 Oaxaca de
Juárez, Oax.
<center>

<img src="resourceMD/croquis.png" alt="croquis">

**Ilustración I** *Croquis de localización* Fuente: [https://goo.gl/maps/xWAJgmqNHsujnnHs5](https://goo.gl/maps/xWAJgmqNHsujnnHs5)

</center>


## 1.7 Organigrama del Negocio.
<center>

```plantuml
@startuml
rectangle "Administrador\ndueño del negocio" as ad
rectangle "Empleado\nmecanico" as emp
rectangle "Empleado\nvendedor" as empV

ad --> emp 
ad --> empV 
@enduml
```

</center>


## 1.8 Breve descripción de los procesos del negocio.
### Venta de llantas y baterías.
El registro de las ventas de las llantas o las baterías se realiza utilizando una libreta y eso en algunas ocasiones suele ser caótico, las notas de las ventas son entregadas a los clientes que lo deseen.
### Adquirir llantas y baterías a proveedores
La comunicación con los proveedores es por llamada telefónica, para solicitar llantas o baterías.

### Inventario.
El registro del conteo de las llantas o baterías que aún se encuentran en almacén,
se realiza de forma manual y es anotado en una libreta.

### Servicio de cambio de llantas o baterías

El servicio de cambio de llantas y baterías se le es cobrado a los clientes cuento
este termina, si el cliente lo requiere se le entrega una nota con las
especificaciones del servicio.

## 1.9 Planteamiento del problema.

Con la descripción de los procesos manuales se identificaron los siguientes problemas: el primero consiste en que el registro de las ventas de las llantas o baterías de forma manual, siendo este anotado en una libreta, lo que ha provocado que en muchas ocasiones los ingresos no coincidan con el corte de caja al final del día, por lo tanto existe inconsistencia de datos.

El cálculo de las ventas de las llantas está a cargo del dueño del local, sin embargo, sus operaciones a veces no corresponden a una adecuada toma de decisiones, debido a que los registros son manuales, por lo que se presentan inconsistencias en los datos de las ventas.

Existe una pérdida de tiempo a la hora de realizar una compra a los proveedores, puesto que, el conteo tanto de las llantas como de las baterías es manual y solo el empleado que haya hecho el registro conoce cuánto queda de existencia en almacén, lo que produce una inexactitud en la cantidad a comprar.

El conteo de las llantas o baterías que aún se encuentran en almacén, se realiza de forma manual siendo anotado en una libreta, El encargado debe de verificar de forma presencial la existencia del producto anotado en la libreta.

El empleado que realiza las llamadas a los proveedores para abastecerse de los productos que la empresa necesite, los empleados deben ir tomando el registro de lo que vayan diciendo los proveedores, además deben tener una nota de lo que los productos que hagan falta. Para verificar qué productos hacen falta, los empleados deben revisar la bodega de forma visual para revisar qué productos hacen falta y muchas veces esto es muy ineficiente.


## 1.10 Objetivos.

### 1.10.1 Objetivo general.
Desarrollar un sistema web para el control de inventarios, ventas de llantas, baterías, servicios y proveedores.

### 1.10.2 Objetivos especificos.
* Identificar los procesos manuales del negocio.
* Realizar el planteamiento del problema.
* Realizar el modelo de requisitos.
* Describir los casos de uso mediante diagramas UML.
* Identificar los requerimientos del negocio.
* Aplicar el modelo ágil en programación extrema (XP) para el desarrollo del proyecto.
* Hacer un plan de reuniones asociadas con las fechas y actividades para la revisión.
* Diseñar el cronograma de actividades
* Estimar costos de desarrollo del software.
* Estimar costo-beneficio de la implementación del proyecto.
* Crear los modelos entidad- relación.
* Crear los modelos relacional
* Utilizar la metodología de gestión de proyectos (la del temario.)
* Utilizar el framework laravel basado en php.
* Crear los diferentes módulos del sistema.
* Crear bases de datos utilizando el gestor de bases de datos MySQL.
* Generar la documentación del sistema.
* Implementar el sistema

## 1.12 Justificación.

Parte de los beneficios que tendrá el desarrollar el sistema web es que mostrará un módulo de ventas, el cual podrá ser accedido mediante un nombre de usuario y contraseña, dicho módulo registrará las ventas realizadas en el día, esto resolvería el problema del conteo manual de las ganancias de las ventas.

Para la satisfacción del cliente, podrá tener la información actualizada en tiempo y forma, además que desde su sistema web podrá gestionar mejor las cantidades
de llantas que tiene disponibles, a su vez podrá tener la información de los proveedores en el sistema agilizando el pedido de las llantas.

Con la implementación del sistema le permitirá al negocio aumentar la productividad y mejorar el trato con los clientes.

También se pretende que el sistema ayude a tener una mejor eficiencia con los registros en los inventarios, para así tener un mejor panorama de los productos.


## 1.13 Alcances y limitaciones

### 1.13.1 Alcances.
El sistema web a desarrollar contendrá los siguientes módulos:
1. Inventario:
* Verificará la cantidad de los productos que se encuentren en el inventario.
* Indicará a los usuarios del sistema la cantidad nueva de productos que se ha añadido al inventario.

2. Ventas:
* El sistema permitirá el registro de ventas que sean realizadas.
* El sistema actualizará el registro de la cantidad de los productos en tiempo real.
* El sistema podrá borrar la información de una venta de ser
necesario.


3. Proveedores:
* El sistema registrará la compra que se le realice a un proveedor.
* Se podrá modificar la información de los proveedores si es
necesario.
* El sistema permitirá borrar la información de un proveedor de ser necesario.

4. Clientes:
* El sistema registrará la información básica del cliente para emitir el ticket de compra en caso de ser necesario.
* El sistema borrará la información de los clientes en caso de ser necesario.
* El sistema podrá actualizar la información de los clientes en caso de ser necesario.

5. Extras:
* El proyecto de la aplicación web se realizará en base a la
microempresa “El llantero de Oaxaca”.
* El sistema será diseñado como una aplicación web con el fin de cumplir con el objetivo de solucionar los problemas que los procesos manuales generan.

### 1.13.2 Limitaciones.
* Los procesos pueden cambiar con el tiempo
* El cobro de ventas se realizará en efectivo.
* Poco manejo de equipos de cómputo del personal del negocio.
* No cuenta con ventas en línea.
* Por políticas de la empresa no cuenta con servicio a domicilio.
* El sistema solo puede ser accedido por personal autorizado.
* El sistema no será accedido mediante conexión a internet.
* Las políticas del negocio impiden la devolución de algunos productos.
* El sistema no podrá generar facturas.
* El sistema solo podrá generar ticket de las compras.

# Capitulo II - Marco teórico.
<table>
    <tr>
        <td>
            sdcsdcsdc
        </td>  
        <td>
            sdcsdcsdc
        </td>  
        <td>
            sdcsdcsdc
        </td>  
        <td>
            sdcsdcsdc
        </td>  
        <td>
            sdcsdcsdc
        </td>  
    </tr>
    <tr> 
        <td colspan="3">
            sdcsdcsdc
        </td>  
        <td>
            sdcsdcsdc
        </td>  
        <td>
            sdcsdcsdc
        </td>  
    </tr>
    <tr>  
    </tr>
<table>

# Desarrollo de la metodologia.